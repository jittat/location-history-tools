export default {
  filterDisplayLocations,
};

function filterDisplayLocations(locations) {
  var displayLocations = [];
  const numDisplay = 300;
  var pr = 1;
  if(locations.length > numDisplay) {
    pr = numDisplay / locations.length;
  }
  locations.forEach(loc => {
    if(Math.random() < pr) {
      displayLocations.push(loc);
    }
  });
  return displayLocations;
}
