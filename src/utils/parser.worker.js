function skipPrefix(data) {
  var i = 0;
  while(data.charAt(i) != '[') {
    i++;
  }
  while(data.charAt(i) != '{') {
    i++;
  }
  return i;
}

function findLast(data, i) {
  var bcount = 1;
  const l = data.length;
  i++;

  for(;;) {
    if(i == l) {
      return i-1;
    }
    var ch = data.charAt(i);
    if(ch == '}') {
      bcount--;
      if(bcount == 0) {
        return i;
      }
    } else if(ch == '{') {
      bcount++;
    } else if(ch == '"') {
      i++;
      for(;;) {
        var cp = data.charAt(i);
        if(cp == '"') {
          break;
        }
        if(cp == '\\') {
          i++;
        }
        i++;
      }
    }
    i++;
  }
}

addEventListener('message', event => {
  const data = event.data.jsonStr;
  const afterTimestamp = event.data.afterTimestamp;
  var lcount = 0;
  var scount = 0;
  var savedLocations = [];

  var start = skipPrefix(data);
  var current = start;
  for(;;) {
    var end = findLast(data, current);
    const objStr = data.slice(current, end+1);
    var obj = JSON.parse(objStr); 
    lcount++;

    if(lcount % 10000 == 0) {
      postMessage({
        status: 'progress',
        readLocationCount: lcount
      });
    }

    if(obj.timestampMs >= afterTimestamp) {
      savedLocations.push(obj);
      scount++;
    }

    current = end;
    var isLast = false;
    for(;;) {
      if(data.charAt(current) == ']') {
        isLast = true;
        break;
      } else if(data.charAt(current) == '{') {
        break;
      }
      current++;
    }
    if(isLast) {
      // console.log(obj);
      break;
    }
  }
  postMessage({
    status: 'done',
    readLocationCount: lcount,
    savedLocationCount: scount,
    savedLocations: savedLocations,
  });
});