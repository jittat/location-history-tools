const WorkerPlugin = require('worker-plugin');

module.exports = {
  parallel: false,
  chainWebpack: config => {
    config
      .plugin('worker-plugin')
      .use(WorkerPlugin)
  },
  publicPath: process.env.NODE_ENV === 'production'
    ? '/' + process.env.CI_PROJECT_NAME + '/'
    : '/'
}
